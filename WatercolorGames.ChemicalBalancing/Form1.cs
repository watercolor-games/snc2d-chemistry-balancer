﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WatercolorGames.ChemicalBalancing
{
    public partial class Form1 : Form
    {
        private Timer _uiTick = new Timer();
        private int _secondsLeft = 60;
        private bool _gameRunning = false;
        private Timer _gameTimer = new Timer();
        private List<Equation> _levels = new List<Equation>();

        private string _summary = "";
        private Skeletal _side1 = null;
        private Skeletal _side2 = null;

        private Font _dispCount = new Font("Tahoma", 11f, FontStyle.Bold);
        private Font _dispChem = new Font("Tahoma", 11f, FontStyle.Regular);
        private int _levelsPassed = 0;

        private int _level = 0;

        public Form1()
        {
            InitializeComponent();
            cbdifficulty.SelectedIndex = 0;
            var scale = Properties.Resources.balance_scale.GetHicon();
            Icon = Icon.FromHandle(scale);
            Text = "Chemical Formula Balancing Game";
            StartPosition = FormStartPosition.CenterScreen;
            _uiTick = new Timer();
            _uiTick.Interval = 100;
            _uiTick.Tick += _uiTick_Tick;
            _gameTimer.Interval = 1000;
            _gameTimer.Tick += _gameTimer_Tick;
            _levels = JsonConvert.DeserializeObject<List<Equation>>(Properties.Resources.Levels);
        }

        private void _gameTimer_Tick(object sender, EventArgs e)
        {
            if (_gameRunning == true)
            {
                if(_secondsLeft==0)
                {
                    _gameRunning = false;
                    _gameTimer.Stop();
                }
                _secondsLeft--;
                lbtitle.Text = $"{_secondsLeft} Seconds Left";
            }
        }

        private void resetCounts()
        {
            flside1.Controls.Clear();
            foreach(var atom in _side1.GetAllSymbols())
            {
                int count = _side1.GetTotalAtomCount(atom);
                var countLabel = new Label();
                var chemLabel = new Label();
                countLabel.Text = count.ToString();
                chemLabel.Text = (count > 1) ? $" {atom} atoms" : $"{atom} atom";
                countLabel.Margin = Padding.Empty;
                chemLabel.Margin = Padding.Empty;

                countLabel.AutoSize = true;
                chemLabel.AutoSize = true;
                countLabel.Font = _dispCount;
                chemLabel.Font = _dispChem;
                flside1.Controls.Add(countLabel);
                flside1.Controls.Add(chemLabel);
                flside1.SetFlowBreak(chemLabel, true);
            }

            flside2.Controls.Clear();
            foreach (var atom in _side2.GetAllSymbols())
            {
                int count = _side2.GetTotalAtomCount(atom);
                var countLabel = new Label();
                var chemLabel = new Label();
                countLabel.Text = count.ToString();
                chemLabel.Text = (count > 1) ? $" {atom} atoms" : $"{atom} atom";
                countLabel.Margin = Padding.Empty;
                chemLabel.Margin = Padding.Empty;

                countLabel.AutoSize = true;
                chemLabel.AutoSize = true;
                countLabel.Font = _dispCount;
                chemLabel.Font = _dispChem;
                flside2.Controls.Add(countLabel);
                flside2.Controls.Add(chemLabel);
                flside2.SetFlowBreak(chemLabel, true);
            }

        }

        private void _uiTick_Tick(object sender, EventArgs e)
        {
            //Move game timer text to top-center
            lbtitle.Top = 15;
            lbtitle.Left = (ClientRectangle.Width - lbtitle.Width) / 2;

            //move the game intro text to proper place
            lbareyousmart.Top = 10;
            lbgamedesc.Top = lbareyousmart.Top + lbareyousmart.Height + 5;
            pnlintro.Width = lbgamedesc.Width + 30;
            lbareyousmart.Left = (pnlintro.Width - lbareyousmart.Width) / 2;
            lbgamedesc.Left = (pnlintro.Width - lbgamedesc.Width) / 2;
            cbdifficulty.Top = lbgamedesc.Top + lbgamedesc.Height + 15;
            btnstartgame.Top = cbdifficulty.Top + cbdifficulty.Height + 5;
            btnstartgame.Left = (pnlintro.Width - btnstartgame.Width) / 2;
            cbdifficulty.Left = (pnlintro.Width - cbdifficulty.Width) / 2;
            pnlintro.Height = btnstartgame.Top + btnstartgame.Height + 10;
            pnlintro.Left = (ClientSize.Width - pnlintro.Width) / 2;
            pnlintro.Top = (ClientSize.Height - pnlintro.Height) / 2;

            lblevel.Top = lbtitle.Top + lbtitle.Height + 5;
            lblevel.Left = (ClientSize.Width - lblevel.Width) / 2;
            lblevel.Text = $"Level {_level + 1}: {_summary}";

            int moleculesY = lblevel.Top + lblevel.Height + 20;

            if(_side1 != null)
            {
                flformula1.Visible = true;
                flformula1.Top = moleculesY;
                 flformula1.Left = ((ClientSize.Width / 2) - flformula1.Width) / 2;
                flside1.Left = 15;
                flside1.Top = moleculesY + flformula1.Height + 5;
                flside1.Visible = true;
            }
            else
            {
                flside1.Visible = false;
                flformula1.Visible = false;
            }

            if (_side2 != null)
            {
                flformula2.Visible = true;
                flformula2.Top = moleculesY;
                flformula2.Left = (ClientSize.Width/2)+(((ClientSize.Width / 2) - flformula2.Width) / 2);
                flside2.Left = (Width - 30) - flside2.Width;
                flside2.Top = moleculesY + flformula2.Height + 5;
                flside2.Visible = true;
            }
            else
            {
                flside2.Visible = false;
                flformula2.Visible = false;
            }


            pnlintro.Visible = !_gameRunning;
            flformula1.Visible = _gameRunning;
            flformula2.Visible = _gameRunning;
            flside1.Visible = _gameRunning;
            flside2.Visible = _gameRunning;
            lblevel.Visible = _gameRunning;
            lbtitle.Visible = _gameRunning;

        }

        private void nextLevel()
        {
            _levelsPassed++;
            if(_level < _levels.Count - 1)
            {
                _level++;
                SetupLevel();
            }
            else
            {
                _gameRunning = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            _uiTick.Start();
            base.OnLoad(e);
        }

        public void SetupLevel()
        {
            var level = _levels[_level];
            Skeletal skSide1 = null;
            string skSide1Error = null;
            Skeletal skSide2 = null;
            string skSide2Error = null;

            if (!ParseFormula(level.Side1, out skSide1, out skSide1Error))
                throw new Exception(skSide1Error);
            if (!ParseFormula(level.Side2, out skSide2, out skSide2Error))
                throw new Exception(skSide2Error);

            _summary = level.Description;
            _side1 = skSide1;
            _side2 = skSide2;

            flformula1.Controls.Clear();
            foreach(var f in _side1.Molecules)
            {
                var nud = new NumericUpDown();
                var text = new Label();
                nud.Maximum = 99;
                nud.Width = 30;
                text.AutoSize = true;
                text.Font = new Font("Times New Roman", 15F, FontStyle.Italic);
                text.Text = f.ToString();
                nud.Minimum = 1;
                nud.Value = f.Count;
                nud.ValueChanged += (o, a) =>
                {
                    f.Count = (int)nud.Value;
                    resetCounts();
                    if (isMatching())
                    {
                        nextLevel();
                    }
                };
                flformula1.Controls.Add(nud);
                flformula1.Controls.Add(text);
            }

            flformula2.Controls.Clear();
            foreach (var f in _side2.Molecules)
            {
                var nud = new NumericUpDown();
                var text = new Label();
                nud.Maximum = 99;
                nud.Width = 30;
                text.AutoSize = true;
                text.Font = new Font("Times New Roman", 15F, FontStyle.Italic);
                text.Text = f.ToString();
                nud.Minimum = 1;
                nud.Value = f.Count;
                nud.ValueChanged += (o, a) =>
                {
                    f.Count = (int)nud.Value;
                    resetCounts();
                    if (isMatching())
                    {
                        nextLevel();
                    }
                };
                flformula2.Controls.Add(nud);
                flformula2.Controls.Add(text);
            }


            resetCounts();
        }

        public bool isMatching()
        {
            bool matches = true;
            foreach(var atom in _side1.GetAllSymbols())
            {
                int s1count = _side1.GetTotalAtomCount(atom);
                int s2count = _side2.GetTotalAtomCount(atom);
                if(s1count != s2count)
                {
                    matches = false;
                    break;
                }
            }
            return matches;
        }

        private void StartGame()
        {
            _levelsPassed = 0;
            _level = 0;
            SetupLevel();
            switch (cbdifficulty.SelectedIndex)
            {
                case 0:
                    _secondsLeft = 120;
                    break;
                case 1:
                    _secondsLeft = 60;
                    break;
                case 2:
                    _secondsLeft = 30;
                    break;
                default:
                    _secondsLeft = 60;
                    break;
            }
            _gameRunning = true;
            _gameTimer.Start();
        }

        private void btnstartgame_Click(object sender, EventArgs e)
        {
            StartGame();
        }

        public int AtomCountParse(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return 1;
            int result = 1;
            int.TryParse(str, out result);
            return result;
        }

        public bool ParseFormula(string inputstr, out Skeletal parsed, out string result)
        {
            Skeletal skeletal = new Skeletal();
            skeletal.Molecules = new List<Formula>();
            foreach (var input in inputstr.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries))
            {
                Formula formula = new Formula();
                formula.Atoms = new List<Atom>();
                string elementId = "";
                bool in_polyatomic = false;
                string count = "";
                for (int i = 0; i < input.Length; i++)
                {
                    char c = input[i];
                    if (char.IsWhiteSpace(c))
                        continue;
                    if (c == '(')
                    {
                        if (in_polyatomic == true)
                        {
                            result = "Error: Nested polyatomic.";
                            parsed = null;
                            return false;
                        }
                        if (!string.IsNullOrWhiteSpace(elementId))
                        {
                            //There's already an element to add.
                            Atom atom = new Atom
                            {
                                ChemSymbol = elementId,
                                Count = AtomCountParse(count)
                            };
                            formula.Atoms.Add(atom);
                            elementId = "";
                            count = "";
                        }

                        in_polyatomic = true;
                        continue;
                    }
                    if (c == ')')
                    {
                        if (in_polyatomic == false)
                        {
                            result = "Error: End of polyatomic, no beginning.";
                            parsed = null;
                            return false;
                        }
                        in_polyatomic = false;
                        continue;
                    }

                    if (char.IsUpper(c))
                    {
                        if (in_polyatomic)
                        {
                            elementId += c;
                            continue;
                        }
                        //Start of a new element.
                        if (!string.IsNullOrWhiteSpace(elementId))
                        {
                            //There's already an element to add.
                            Atom atom = new Atom
                            {
                                ChemSymbol = elementId,
                                Count = AtomCountParse(count)
                            };
                            formula.Atoms.Add(atom);
                            elementId = "";
                            count = "";
                        }
                        elementId += c;
                        continue;
                    }
                    else if (char.IsLower(c))
                    {
                        if (in_polyatomic)
                        {
                            elementId += c;
                            continue;
                        }

                        if (string.IsNullOrWhiteSpace(elementId))
                        {
                            //Malformed element ID.
                            result = "Error: Malformed chemical symbol. Chemical symbols must start with an uppercase letter.";
                            parsed = null;
                            return false;
                        }
                        elementId += c;
                        continue;
                    }
                    else if (char.IsNumber(c))
                    {
                        if (in_polyatomic)
                        {
                            elementId += c;
                            continue;
                        }

                        if (string.IsNullOrWhiteSpace(elementId))
                        {
                            result = "Error: Malformed atom! Atom count cannot precede the atom itself.";
                            parsed = null;
                            return false;
                        }
                        count += c;
                    }
                    else
                    {
                        result = "Malformed formula: Unrecognized character.";
                        parsed = null;
                        return false;
                    }
                }
                //Perform this check once again.
                if (!string.IsNullOrWhiteSpace(elementId))
                {
                    //There's already an element to add.
                    Atom atom = new Atom
                    {
                        ChemSymbol = elementId,
                        Count = AtomCountParse(count)
                    };
                    formula.Atoms.Add(atom);
                    elementId = "";
                    count = "";
                }
                if (in_polyatomic)
                {
                    result = "End of polyatomic expected.";
                    parsed = null;
                    return false;
                }
                skeletal.Molecules.Add(formula);
            }

            result = "success";
            parsed = skeletal;
            return true;
        }

    }

    public class Equation
    {
        public string Description { get; set; }
        public string Side1 { get; set; }
        public string Side2 { get; set; }
    }

    public class Skeletal
    {
        public List<Formula> Molecules { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var m in Molecules)
            {
                if(m.Count > 1)
                    sb.Append(m.Count + " " + m.ToString() + " ");
                else
                    sb.Append(m.ToString() + " ");
            }
            return sb.ToString().Trim();

        }

        public int GetTotalAtomCount(string symbol)
        {
            int count = 0;
            foreach (var m in Molecules)
            {
                int acount = 0;
                foreach (var a in m.Atoms)
                {
                    if (a.ChemSymbol == symbol)
                        acount += a.Count;
                }
                count += acount * m.Count;
            }

            return count;
        }

        public string[] GetAllSymbols()
        {
            List<string> symbols = new List<string>();

            foreach (var m in Molecules)
            {
                foreach(var a in m.Atoms)
                {
                    if (!symbols.Contains(a.ChemSymbol))
                        symbols.Add(a.ChemSymbol);
                }
            }

            return symbols.ToArray();
        }

    }

    public class Formula
    {
        public List<Atom> Atoms { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var atom in Atoms)
                if (atom.Count > 1)
                    sb.Append($"{atom.ChemSymbol}{atom.Count.ToUnicodeSubscript()}");
                else
                    sb.Append(atom.ChemSymbol);
            return sb.ToString();
        }
        internal int Count = 1;
    }

    public class Atom
    {
        public string ChemSymbol { get; set; }
        public int Count { get; set; }
    }

    public static class IntExtensions
    {
        public static string ToUnicodeSubscript(this int value)
        {
            string valuestr = value.ToString();
            string nstr = "";
            foreach(char c in valuestr)
            {
                switch (c)
                {
                    case '0':
                        nstr += "\u2080";
                        break;
                    case '1':
                        nstr += "\u2081";
                        break;
                    case '2':
                        nstr += "\u2082";
                        break;
                    case '3':
                        nstr += "\u2083";
                        break;
                    case '4':
                        nstr += "\u2084";
                        break;
                    case '5':
                        nstr += "\u2085";
                        break;
                    case '6':
                        nstr += "\u2086";
                        break;
                    case '7':
                        nstr += "\u2087";
                        break;
                    case '8':
                        nstr += "\u2088";
                        break;
                    case '9':
                        nstr += "\u2089";
                        break;
                }
            }
            return nstr;
        }
    }
}
