﻿namespace WatercolorGames.ChemicalBalancing
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lbtitle = new System.Windows.Forms.Label();
            this.pnlintro = new System.Windows.Forms.Panel();
            this.btnstartgame = new System.Windows.Forms.Button();
            this.lbgamedesc = new System.Windows.Forms.Label();
            this.lbareyousmart = new System.Windows.Forms.Label();
            this.lblevel = new System.Windows.Forms.Label();
            this.flside1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flside2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flformula1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flformula2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbdifficulty = new System.Windows.Forms.ComboBox();
            this.pnlintro.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbtitle
            // 
            this.lbtitle.AutoSize = true;
            this.lbtitle.Font = new System.Drawing.Font("Tahoma", 13.5F, System.Drawing.FontStyle.Bold);
            this.lbtitle.Location = new System.Drawing.Point(43, 13);
            this.lbtitle.Name = "lbtitle";
            this.lbtitle.Size = new System.Drawing.Size(152, 22);
            this.lbtitle.TabIndex = 0;
            this.lbtitle.Text = "60 Seconds Left";
            // 
            // pnlintro
            // 
            this.pnlintro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlintro.Controls.Add(this.cbdifficulty);
            this.pnlintro.Controls.Add(this.btnstartgame);
            this.pnlintro.Controls.Add(this.lbgamedesc);
            this.pnlintro.Controls.Add(this.lbareyousmart);
            this.pnlintro.Location = new System.Drawing.Point(444, 142);
            this.pnlintro.Name = "pnlintro";
            this.pnlintro.Size = new System.Drawing.Size(407, 164);
            this.pnlintro.TabIndex = 1;
            // 
            // btnstartgame
            // 
            this.btnstartgame.Location = new System.Drawing.Point(215, 107);
            this.btnstartgame.Name = "btnstartgame";
            this.btnstartgame.Size = new System.Drawing.Size(75, 23);
            this.btnstartgame.TabIndex = 2;
            this.btnstartgame.Text = "Start";
            this.btnstartgame.UseVisualStyleBackColor = true;
            this.btnstartgame.Click += new System.EventHandler(this.btnstartgame_Click);
            // 
            // lbgamedesc
            // 
            this.lbgamedesc.AutoSize = true;
            this.lbgamedesc.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbgamedesc.Location = new System.Drawing.Point(94, 63);
            this.lbgamedesc.MaximumSize = new System.Drawing.Size(350, 0);
            this.lbgamedesc.Name = "lbgamedesc";
            this.lbgamedesc.Size = new System.Drawing.Size(349, 126);
            this.lbgamedesc.TabIndex = 2;
            this.lbgamedesc.Text = resources.GetString("lbgamedesc.Text");
            this.lbgamedesc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbareyousmart
            // 
            this.lbareyousmart.AutoSize = true;
            this.lbareyousmart.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lbareyousmart.Location = new System.Drawing.Point(21, 11);
            this.lbareyousmart.MaximumSize = new System.Drawing.Size(350, 0);
            this.lbareyousmart.Name = "lbareyousmart";
            this.lbareyousmart.Size = new System.Drawing.Size(342, 36);
            this.lbareyousmart.TabIndex = 1;
            this.lbareyousmart.Text = "Are you smarter than a 10th grade chemistry student?";
            this.lbareyousmart.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblevel
            // 
            this.lblevel.AutoSize = true;
            this.lblevel.Font = new System.Drawing.Font("Tahoma", 11.5F, System.Drawing.FontStyle.Bold);
            this.lblevel.Location = new System.Drawing.Point(204, 48);
            this.lblevel.Name = "lblevel";
            this.lblevel.Size = new System.Drawing.Size(137, 19);
            this.lblevel.TabIndex = 2;
            this.lblevel.Text = "60 Seconds Left";
            // 
            // flside1
            // 
            this.flside1.AutoSize = true;
            this.flside1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flside1.Location = new System.Drawing.Point(12, 128);
            this.flside1.Name = "flside1";
            this.flside1.Size = new System.Drawing.Size(0, 0);
            this.flside1.TabIndex = 5;
            // 
            // flside2
            // 
            this.flside2.AutoSize = true;
            this.flside2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flside2.Location = new System.Drawing.Point(442, 225);
            this.flside2.Name = "flside2";
            this.flside2.Size = new System.Drawing.Size(0, 0);
            this.flside2.TabIndex = 6;
            // 
            // flformula1
            // 
            this.flformula1.AutoSize = true;
            this.flformula1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flformula1.Location = new System.Drawing.Point(0, 0);
            this.flformula1.Name = "flformula1";
            this.flformula1.Size = new System.Drawing.Size(0, 0);
            this.flformula1.TabIndex = 6;
            // 
            // flformula2
            // 
            this.flformula2.AutoSize = true;
            this.flformula2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flformula2.Location = new System.Drawing.Point(0, 0);
            this.flformula2.Name = "flformula2";
            this.flformula2.Size = new System.Drawing.Size(0, 0);
            this.flformula2.TabIndex = 6;
            // 
            // cbdifficulty
            // 
            this.cbdifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbdifficulty.FormattingEnabled = true;
            this.cbdifficulty.Items.AddRange(new object[] {
            "Casual (2 minutes)",
            "Normal (60 seconds)",
            "Insane (30 seconds)"});
            this.cbdifficulty.Location = new System.Drawing.Point(188, 136);
            this.cbdifficulty.Name = "cbdifficulty";
            this.cbdifficulty.Size = new System.Drawing.Size(121, 21);
            this.cbdifficulty.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 451);
            this.Controls.Add(this.flformula1);
            this.Controls.Add(this.flformula2);
            this.Controls.Add(this.flside2);
            this.Controls.Add(this.flside1);
            this.Controls.Add(this.pnlintro);
            this.Controls.Add(this.lbtitle);
            this.Controls.Add(this.lblevel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pnlintro.ResumeLayout(false);
            this.pnlintro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbtitle;
        private System.Windows.Forms.Panel pnlintro;
        private System.Windows.Forms.Label lbgamedesc;
        private System.Windows.Forms.Label lbareyousmart;
        private System.Windows.Forms.Button btnstartgame;
        private System.Windows.Forms.Label lblevel;
        private System.Windows.Forms.FlowLayoutPanel flside1;
        private System.Windows.Forms.FlowLayoutPanel flside2;
        private System.Windows.Forms.FlowLayoutPanel flformula1;
        private System.Windows.Forms.FlowLayoutPanel flformula2;
        private System.Windows.Forms.ComboBox cbdifficulty;
    }
}

